/*

  Andy McEvoy

  mcevoy.andy@gmail.com

  Sketch to control an RGB LED strip with 4 potentiometers.
  One pot to control R, G, B, and the brightness of the LED Strip.

*/
 
// Define RGB pins, must use PWM pins.
#define RED_PIN 9
#define GREEN_PIN 10
#define BLUE_PIN 11

#define BRIGHTNESS_CTRL 1
#define RED_CTRL 2
#define GREEN_CTRL 3
#define BLUE_CTRL 4

//#define DEBUG

const double R2=560.0; // Value of resistor 2 in the voltage divider
const double R1_MAX=100; // Maximum value for the potentiometer's resistance
const double THRESHOLD=10.0; // Threshold for max/min values on pot
const double RAINBOW_DELAY=5.0;
const double RAINBOW_STEPS_PER_COLOR=100.0;

// Define ROYGBIV
int rainbow[7][3]={{0}};

// Prototypes:

double get_RGB_set_value(int pin, double brightness); // get RGB values indicated by the potentiometers
int RGB_inputs_changed(); // check the RGB inputs, used to interrupt rainbow sequence
void cycle_rainbow_colors(); // continuously cycle through the colors of the rainbow

void setup() {
#ifdef DEBUG
  Serial.begin(9600);
#endif
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  rainbow[0][0]=255; rainbow[0][1]=0; rainbow[0][2]=0; // red
  rainbow[1][0]=255; rainbow[1][1]=165; rainbow[1][2]=0; // orange
  rainbow[2][0]=255; rainbow[2][1]=255; rainbow[2][2]=0; // yellow
  rainbow[3][0]=0; rainbow[3][1]=255; rainbow[3][2]=0; // green
  rainbow[4][0]=0; rainbow[4][1]=0; rainbow[4][2]=255; // blue
  rainbow[5][0]=75; rainbow[5][1]=0; rainbow[5][2]=130; // indigo
  rainbow[6][0]=238; rainbow[6][1]=130; rainbow[6][2]=238; // violet

}
 
 
void loop() {
  double brightness=get_RGB_set_value(BRIGHTNESS_CTRL,1.0);
  double r=get_RGB_set_value(RED_CTRL,(brightness/255.0));
  double g=get_RGB_set_value(GREEN_CTRL,(brightness/255.0));
  double b=get_RGB_set_value(BLUE_CTRL,(brightness/255.0));

#ifdef DEBUG
  Serial.print("RGB = [");
  Serial.print((int)r);
  Serial.print(",");
  Serial.print((int)g);
  Serial.print(",");
  Serial.print((int)b);
  Serial.print("]");
  Serial.print(" brightness = ");
  Serial.println(brightness);
#endif

  if ((int)r==0 && (int)g==0 && (int)b==0 && (int)brightness==255) cycle_rainbow_colors();

  analogWrite(RED_PIN,(int)r);
  analogWrite(GREEN_PIN,(int)g);
  analogWrite(BLUE_PIN,(int)b);

#ifdef DEBUG
  delay(500);
#endif
}

int RGB_inputs_changed() {
  double brightness=get_RGB_set_value(BRIGHTNESS_CTRL,1.0);
  double r=get_RGB_set_value(RED_CTRL,(brightness/255.0));
  double g=get_RGB_set_value(GREEN_CTRL,(brightness/255.0));
  double b=get_RGB_set_value(BLUE_CTRL,(brightness/255.0));
  if ((int)r==0 && (int)g==0 && (int)b==0) return 0; // inputs have not been changed
  else return 1;
}

void cycle_rainbow_colors() {
  int i=0; // start with red
  double current_color[3], next_color[3], change_rate[3];
  int step_delay=(int)((RAINBOW_DELAY/RAINBOW_STEPS_PER_COLOR)*1000);
#ifdef DEBUG
  Serial.print("Step delay = ");
  Serial.print(step_delay);
#endif
  current_color[0]=rainbow[0][0];
  current_color[1]=rainbow[0][1];
  current_color[2]=rainbow[0][2];

  analogWrite(RED_PIN,(int)current_color[0]);
  analogWrite(GREEN_PIN,(int)current_color[1]);
  analogWrite(BLUE_PIN,(int)current_color[2]);

  while (RGB_inputs_changed()==0) {
    if ((i+1)==7) i=0;
    else i+=1;    
    next_color[0]=rainbow[i][0];
    next_color[1]=rainbow[i][1];
    next_color[2]=rainbow[i][2];
    change_rate[0]=(next_color[0]-current_color[0])/(RAINBOW_STEPS_PER_COLOR);
    change_rate[1]=(next_color[1]-current_color[1])/(RAINBOW_STEPS_PER_COLOR);
    change_rate[2]=(next_color[2]-current_color[2])/(RAINBOW_STEPS_PER_COLOR);
    
#ifdef DEBUG
    Serial.print("i = ");
    Serial.print(i);
    Serial.print("\tnext color = [");
    Serial.print((int)next_color[0]); Serial.print(", ");
    Serial.print((int)next_color[1]); Serial.print(", ");
    Serial.print((int)next_color[2]); Serial.println("]");
#endif

    for (int j=0; j<RAINBOW_STEPS_PER_COLOR; j++) {
      current_color[0]+=change_rate[0];
      current_color[1]+=change_rate[1];
      current_color[2]+=change_rate[2];      

#ifdef DEBUG
    Serial.print("\tj = ");
    Serial.print(j);
    Serial.print("\t\tcurrent color = [");
    Serial.print((int)current_color[0]); Serial.print(", ");
    Serial.print((int)current_color[1]); Serial.print(", ");
    Serial.print((int)current_color[2]); Serial.println("]");
#endif
      
      analogWrite(RED_PIN,(int)current_color[0]);
      analogWrite(GREEN_PIN,(int)current_color[1]);
      analogWrite(BLUE_PIN,(int)current_color[2]);

      delay(step_delay);
      if (RGB_inputs_changed()==1) return;
    }
  }
}

double get_RGB_set_value(int pin, double brightness) {
  int rawADC;
  double percent, R1;
  rawADC=analogRead(pin);
#ifdef DEBUG_GET_RGB
  Serial.print(rawADC);
  Serial.print(":");
#endif
  if (rawADC==0) return 0; // if reading zero, don't try to calculate R1.
  else {
    R1=(R2*1023.0)/rawADC-R2;
#ifdef DEBUG_GET_RGB
    Serial.print(R1);
    Serial.print(":");
#endif
  }
  if (R1>=(R1_MAX-THRESHOLD)) {
#ifdef DEBUG_GET_RGB
    Serial.print("U");
    Serial.print(", ");
#endif
    return (255.0*brightness); // Above threshold, color is maxed out
  } else if (R1<=THRESHOLD) {
#ifdef DEBUG_GET_RGB
    Serial.print("D");
    Serial.print(", ");
#endif
    return 0.0; // Below threshold, color is off
  } else {
    percent=(R1-THRESHOLD)/(R1_MAX-2*THRESHOLD);
#ifdef DEBUG_GET_RGB
    Serial.print(percent);
    Serial.print(", ");
#endif
  }
  int value=brightness*percent*255.0; // check on set range
  if (value>255) return 255;
  else if (value<0) return 0;
  else return value;
}


/*
   Andy McEvoy
   
   19 - August - 2013
   
   Control for an RGB LED strip.
 */ 

#define F_CPU 20000000

#include <util/delay.h>
#include <avr/io.h>

// ADC selection mask
static const uint8_t MUX_MASK=(1<<MUX5) | (1<<MUX4) | (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0);

static const float R2=1000.0;
static const uint8_t RED_ADC=2; // Note this is ADCx, not PAx or PBx
static const uint8_t GREEN_ADC=3;
static const uint8_t BLUE_ADC=4;

void initialize_PWM(){
	DDRA = (1 << PA7) | (1 << PA6);
	DDRB |= (1 << PB2);

	TCCR0A = (0<<WGM01) | (1<<WGM00) | (1<<COM0A1) | (0<<COM0A0) | (1<<COM0B1) | (0<<COM0B0);	
	TCCR0B = (0<<WGM02) | (0<<CS02) | (0<<CS01) | (1<<CS00) | (0<<FOC0A) | (0<<FOC0B);

	TCCR1A = (0<<WGM11) | (1<<WGM10) | (1<<COM1A1) | (0<<COM1A0) | (1<<COM1B1) | (0<<COM1B0);
	TCCR1B = (0<<WGM12) | (0<<CS12) | (0<<CS11) | (1<<CS10) | (0<<FOC1A) | (0<<FOC1B);
}

void initialize_ADC() {
	ADMUX = (0<<REFS0) | (0<<REFS1);
			
	ADCSRA = (1<<ADEN);
	ADCSRB = (1<<ADLAR);     
}

uint8_t get_adc_value(int adc_pin) {
		float reading=0.0;
		
		ADMUX=(ADMUX & ~(MUX_MASK)) | (adc_pin & MUX_MASK); // clear previous ADC selection, set new selection

		ADCSRA |= (1<<ADSC); // ready to start a reading, automatically returns to 0 when done
		while (ADCSRA & (1<<ADSC)) {} // do nothing
		
		reading=ADCH;
		float R1=R2*255.0/reading-R2; // voltage divider equation
		
		//threshold value, R_variable = ~0 to ~10k Ohms		
		if (R1>9500.0) return 255;
		if (R1<500.0) return 0;
		
		R1=(R1-500.0)/9000.0*255.0; // scale to 0-255
		
		return (uint8_t)R1;
}

void run_rainbow() {
	uint8_t rgb[3];
	uint8_t test[3]={0};
	
	rgb[0]=0;
	rgb[1]=255;
	rgb[2]=0;
	
	int addened=1;
	
	OCR1A=rgb[0]; // set initial color scheme for rainbow loop
	OCR0A=rgb[1];
	OCR0B=rgb[2];
	
	for (;;) { 
		for (uint8_t i=0; i<3; i++) { // cycle through colors
			for (uint8_t j=0; j<255; j++) { // cycle through all values color can have
				rgb[i] += addened; // go up to 255 or down to zero
				OCR1A=rgb[0];
				OCR0A=rgb[1];
				OCR0B=rgb[2];
				_delay_ms(1); // loop through colors fast
				
				test[0]=get_adc_value(RED_ADC); // check if sliders have moved 
				test[1]=get_adc_value(GREEN_ADC);
				test[2]=get_adc_value(BLUE_ADC);
				if (test[0]!=0 || test[1]!=0 || test[2]!=0) return;
			}
			addened *= -1; // switch whether next color goes up to 255 or down to 0
		}
	}
}

int main( void ){
	initialize_PWM();
	initialize_ADC();
	uint8_t rgb[3];

	rgb[0]=0; // set led strip off when starting up
	rgb[1]=0; 
	rgb[2]=0; 
	
	while(1) {
		
		rgb[0]=get_adc_value(RED_ADC);
		rgb[1]=get_adc_value(GREEN_ADC);
		rgb[2]=get_adc_value(BLUE_ADC);
		
		if (rgb[0]==0 && rgb[1]==0 && rgb[2]==0) run_rainbow();
		
		OCR1A=rgb[0]; // set colors
		OCR0A=rgb[1];
		OCR0B=rgb[2];
	}
}